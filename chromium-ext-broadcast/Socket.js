        
/**
 * Object socket
 * 
 * @param callback
 * @constructor
 */
function Socket(type, callback, console) {
	var self = this;
	this._console = console;
	this._host    = '';
	this._port    = 0;
	this.info    = {};
	//console.log(this)
	chrome.socket.create(type, {}, function(socketInfo) {
		//self._console && self._console.log('[TcpSocket]', 'create', socketInfo.socketId);
		self.info = socketInfo;
		callback && callback(socketInfo);
	});
}


/**
 * Connect interceptor
 * 
 * @param host
 * @param port
 * @param callback
 */
Socket.prototype.connect = function(host, port, callback) {
	var self = this;
	this._host = host;
	this._port = parseInt(port);
	chrome.socket.connect(this.info.socketId, this._host, this._port, function(result) {
		callback && callback(result);
	});
};

/**
 * Write interceptor
 * 
 * @param string
 * @param callback
 */
Socket.prototype.write = function(string, callback) {
	var self = this;
	chrome.socket.write(this.info.socketId, Socket.string2buffer(string), function(writeInfo) {
		//self._console && self._console.log('[TcpSocket]', 'write', self._host, self._port, string.replace(/[\r\n\t]/g, '.'));
//		chrome.socket.disconnect(self.info.socketId);
		callback && callback(writeInfo);
	});
};

/**
 * Read interceptor
 * 
 * @param callback
 */
Socket.prototype.read = function(callback) {
	var self = this;
	chrome.socket.read(this.info.socketId, function(readInfo) {
		//console.log('READINFO', arguments);
		var string = Socket.buffer2string(readInfo.data);
		//self._console && self._console.log('[TcpSocket]', 'read', self._host, self._port, string.replace(/[\r\n\t]/g, '.'));
//		chrome.socket.disconnect(self.info.socketId);
		callback && callback(string);
	});
};

/**
 * Streamer reader from socket
 * 
 * @param callback
 */

Socket.prototype.stream = function(callback) {
	var self = this;
	var blob = '';
	
	var handler = function(data) {
		if (!data.length) {
			//EOF
			self.disconnect();
			callback && callback(blob);
		} else {
			blob += data;
			self.read(handler);
		}
	};
	
	this.read(handler);
};


/**
 * Streamer writer to socket
 * 
 * @param callback
 */
//non blocking. heavy on browser, for small/medium files only <80MB
Socket.prototype.streamwrite_nb = function(blob, callback) {

    var progress = document.querySelector('.percent')
    document.getElementById('percent').className = 'percent'
    var blob = blob
	var self = this;
	var file_length = blob.length
	var self = this;
	var chunk = ''
//	var start=0
//	var end=8192 //transfer block size
	var percentLoaded = 0
    var where_to = this.info.socketId
    console && console.log("[Socket] ", where_to)

    function update(blob,callback){ //support function called from async looper
        console && console.log("[update] ", where_to)

        chrome.socket.write(where_to, Socket.string2buffer(blob), function(writeInfo) {
        percentLoaded = Math.round((end/file_length)*100)
  
  	    if (percentLoaded<100) {
        progress.style.width = percentLoaded + '%';
        progress.textContent = percentLoaded + '%';
	}

    //console.log(percentLoaded)
  });

    callback && callback(blob)
    }


asyncForEach(blob, function(el, callback) { //initializer for async looping
  update(el, function() { // my callback
    callback();
  });
}, function() {
    //console && console.log("Done!");
    progress.style.width = '100%';
    progress.textContent = '100%';
    setTimeout(function(){
        //document.querySelector('.percent').className = '';
        progress.textContent = 'Done';
        console && console.log(where_to, "Done")

	},2000)
       	//chrome.socket.disconnect(where_to);
        callback && callback()	

});

};

//non blocking. heavy on browser, for small/medium files only <80MB
Socket.prototype.streamwrite_block = function(blob, callback) {


      var progress = document.querySelector('.percent')

        var self = this;
//        var cycles = Math.round(blob.length / (20*1023))
        var self = this;
        var chunk = ''
        var start=0
        var end=8192
        var percentLoaded = 0
        var where_to = this.info.socketId


do
  {
        chunk = blob.slice(start,end)
        //self._console && self._console.log(start, end, chunk.length);

        start =end
        end =end + 8192

        percentLoaded = Math.round((end/blob.length)*100)
      // Increase the progress bar length.


//        self._console && self._console.log(percentLoaded);

        chrome.socket.write(this.info.socketId, Socket.string2buffer(chunk), function(writeInfo) {

        });

  }
while (chunk.length>0);

    progress.style.width = '100%';
    progress.textContent = '100%';

    setTimeout(function(){
        progress.textContent = 'Done';
        console && console.log(where_to, "Done")

	},2000)
       	chrome.socket.disconnect(where_to);
        callback && callback()
};


Socket.prototype.disconnect = function() {
	var self = this;
	chrome.socket.disconnect(this.info.socketId);
};

/**
 * Convert ArrayBuffer to string
 * 
 * @param buffer
 * @return {Object}
 */
Socket.buffer2string = function(buffer) {
	return String.fromCharCode.apply(null, new Uint8Array(buffer));
};

/**
 * Convert string to ArrayBuffer
 * 
 * @param string
 * @return {ArrayBuffer}
 */
Socket.string2buffer = function(string) {
	var buffer = new ArrayBuffer(string.length);
	var view = new Uint8Array(buffer);
	var chars = string.split('');
	chars.map(function(value, index, array) {
		view[index] = value.charCodeAt();
	});
	return buffer;
};

function str2ab(str) {
	var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
	var bufView = new Uint16Array(buf);
	for (var i=0, strLen=str.length; i<strLen; i++) {
		bufView[i] = str.charCodeAt(i);
	}
	return buf;
}

function asyncForEach(array, fn, callback) { //async looper
  var array = array.slice(0);
  var start=0
  var end = 64*1024
  
  function processOne() {
    var item = array.slice(start,end);
    start =end
    end =end + 64*1024
    fn(item, function(result) {

        if(item.length > 0) {
          setTimeout(processOne, 0); // schedule immediately

        } else {
          callback(); // Done!
        }
      });
  }
  if(array.length > 0) {
    setTimeout(processOne, 0); // schedule immediately
  } else {
    callback(); // Done!
  }
};

//module.exports = Socket;
