/**
 * Poor man's FTP client
 * 
 * @param {String} host
 * @param {Number} port
 * @param {Object} console
 * @constructor
 */
//var Socket = require('./Socket');


function Ftp(host, port, console, username, password) {
	this.console = console;
	this.host    = host;
	this.port    = parseInt(port);
	this.username = username || "anonymous";
	this.password = password || 'anonymous';
	this.usernamestring = 'USER '+ this.username + '\r\n';
	this.passwordstring = 'PASS ' + this.password + '\r\n';

}

/**
 * Convert passive response to port number
 * 
 * @param {String} pasv
 * @return {Number}
 */
Ftp.pasv2port = function(pasv) {
	//227 PASV ok (192,168,1,1,129,178)
	var pasvs = pasv.match(/\d+/g); //["227", "192", "168", "1", "1", "129", "178"]
	//console.log("passss ", pasv, pasvs, parseInt(pasvs[6]) + 256 * parseInt(pasvs[5]))
	return parseInt(pasvs[6]) + 256 * parseInt(pasvs[5]); //33202
};


/**
 * Download a file
 * 
 * @param {String} path
 * @param {Function} callback
 */
Ftp.prototype.retrieve = function(path, eofHandler) {
	var self = this;
	var controlSocket, binarySocket; //commands usually 21, transfers usually > 1023
	//callback hell
	this.console && this.console.log('[Ftp]', 'retrieve', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
		//this.console && this.console.log("nahazuji socket: ", self)
			controlSocket.read(function(data) {
				controlSocket.write(self.usernamestring, function() {
					controlSocket.read(function(data) {
						controlSocket.write(self.passwordstring, function() {
							controlSocket.read(function(data) {
								controlSocket.write('PASV\n', function() {
									controlSocket.read(function(pasv) {
										binarySocket = new Socket('tcp', function() {
											binarySocket.connect(self.host, Ftp.pasv2port(pasv), function() {
												controlSocket.write('RETR ' + path + '\r\n', function() {
													binarySocket.stream(eofHandler);
													controlSocket.disconnect();
												});
											});
										}, self.console);
									});
								});
							});
						});
					});
				});			

			});
		});
	}, self.console);
};

/**
 * Upload a file
 * 
 * @param {String} file blob
 * @param {Function} callback
 */
 
 //non blocking upload style. good for small to medium files (< 80MB)
Ftp.prototype.upload_nb = function(blob, path, callback) {
	var self = this;
	var controlSocket, binarySocket; //commands usually 21, transfers usually > 1023
	//callback hell
	this.console && this.console.log('[Ftp]', 'upload',  path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
		this.console && this.console.log("nahazuji socket: ", self)
			controlSocket.read(function(data) {
				controlSocket.write(self.usernamestring, function() {
					controlSocket.read(function(data) {
						controlSocket.write(self.passwordstring, function() {
							controlSocket.read(function(data) {
								controlSocket.write('PASV\n', function() {
									controlSocket.read(function(pasv) {
										binarySocket = new Socket('tcp', function() {
											binarySocket.connect(self.host, Ftp.pasv2port(pasv), function() {
											    this.console && this.console.log("[ftp] ", binarySocket.socketId, path)
												controlSocket.write('STOR ' + path + '\r\n', function() {
												//this.console && this.console.log(path)
												   //console.log(blob)
													binarySocket.streamwrite_nb(blob,function(writeInfo){
														binarySocket.disconnect();
														controlSocket.disconnect();
														this.console && this.console.log("nb ", writeInfo);
														callback && callback()
														//xxx=writeInfo


													});
												});
											});
										}, self.console);
									});
								});
							});
						});
					});
				});			

			});
		});
	}, self.console);
};


/**
 * Upload a file
 * 
 * @param {String} file blob
 * @param {Function} callback
 */
 
 //blocking upload style. needed for large files (> 80MB). Will block the GUI
Ftp.prototype.upload_block = function(blob, path, callback) {
	var self = this;
	var controlSocket, binarySocket; //commands usually 21, transfers usually > 1023
	//callback hell
	//this.console && this.console.log('[Ftp]', 'retrieve', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
		this.console && this.console.log("nahazuji socket: ", self)
			controlSocket.read(function(data) {
				controlSocket.write(self.usernamestring, function() {
					controlSocket.read(function(data) {
						controlSocket.write(self.passwordstring, function() {
							controlSocket.read(function(data) {
								controlSocket.write('PASV\n', function() {
									controlSocket.read(function(pasv) {
										binarySocket = new Socket('tcp', function() {
											binarySocket.connect(self.host, Ftp.pasv2port(pasv), function() {
												controlSocket.write('STOR ' + path + '\r\n', function() {
												//this.console && this.console.log(path)
													binarySocket.streamwrite_block(blob,function(writeInfo){
														//binarySocket.disconnect();
														controlSocket.disconnect();
														this.console && this.console.log("block ", writeInfo);
														callback && callback()
														//xxx=writeInfo


													});
												});
											});
										}, self.console);
									});
								});
							});
						});
					});
				});			

			});
		});
	}, self.console);
};


/**
 * Delete a file
 * 
 * @param {String} path
 * @param {Function} callback
 */
Ftp.prototype.deleteFile = function(path, callback) {
	var self = this;
	var controlSocket;
	this.console && this.console.log('[Ftp]', 'delete', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
			controlSocket.read(function() {
				controlSocket.write(self.usernamestring, function() {
					controlSocket.read(function(data) {
						controlSocket.write(self.passwordstring, function() {
							controlSocket.read(function(data) {
                				controlSocket.write('DELE ' + path + '\r\n', function() {
				                	controlSocket.read(function() {
                						controlSocket.disconnect();
                						callback && callback();
                					});
                				});
            				})
        				})
    				})
				})
			});
		});
	}, self.console);
};

/**
 * Remove a directory
 * 
 * @param {String} path
 * @param {Function} callback
 */
Ftp.prototype.removeDirectory = function(path, callback) {
	var self = this;
	var controlSocket;
	this.console && this.console.log('[Ftp]', 'removeDirectory', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
			controlSocket.read(function() {
				controlSocket.write('RMD ' + path + '\r\n', function() {
					controlSocket.read(function() {
						controlSocket.disconnect();
						callback && callback();
					});
				});
			});
		});
	}, self.console);
};

/**
* Get contents of a directory listing
*
* @param {String} path
* @param {Function} callback
*/
Ftp.prototype.listDirectory = function(path, callback) {
	var self = this;
	//console.log("toto: ", this)
	var controlSocket, binarySocket; //commands usually 21, transfers usually > 1023
	//callback hell
	this.console && this.console.log('[Ftp]', 'listDirectory', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
			controlSocket.read(function(data) {
			petr=controlSocket
				controlSocket.write(self.usernamestring, function() {
					controlSocket.read(function(data) {
						controlSocket.write(self.passwordstring, function() {
							controlSocket.read(function(data) {
								controlSocket.write('PASV\r\n', function() {
									controlSocket.read(function(pasv) {
										binarySocket = new Socket('tcp', function() {
											binarySocket.connect(self.host, Ftp.pasv2port(pasv), function() {
												controlSocket.write('NLST ' + path + '\r\n', function() {
													binarySocket.stream(function(blob) {
														var items = [];
														//console.log(blob)
														items=blob.trim().split(/[\r\n]+/)
														//blob.trim().split(/[\r\n]+/).map(function(item, index, array) {
															//sanitize filenames (no spaces please)
														//	item = item.split(/\s+/).pop();
														//	if (!item) return;
														//	items.push(item);
														//});
														callback && callback(items);
													});
													controlSocket.disconnect();
												});
											});
										}, self.console);
									});
								});
							});			
						});
					});
				});
			});
		});
	}, self.console);
};


/**
 * File existence check
 *
 * @param path
 * @param callback
 */
Ftp.prototype.fileExists = function(path, callback) {
	var self = this;
	var controlSocket;
	this.console && this.console.log('[Ftp]', 'fileExists', path);
	controlSocket = new Socket('tcp', function() {
		controlSocket.connect(self.host, self.port, function() {
			controlSocket.read(function() {
				controlSocket.write('SIZE ' + path + '\r\n', function() {
					controlSocket.read(function(response) {
						controlSocket.disconnect();
						var size = response.trim().split(' ')[1];
						callback && callback('Error' != size);
					});
				});
			});
		});
	}, self.console);
};

//module.exports = Ftp;
