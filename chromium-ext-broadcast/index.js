/*
   Copyright 2012 Google Inc
  Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
//   && console && console.log
onload = function () {

    minimes = [] //array of hosts found  on the network
    document.update_servers = [] //checked servers
    document.erasing=0
    document.erase_all=0

	//window.indexedDB = window.indexedDB
	//IDBTransaction = window.IDBTransaction

	dbVersion = 5;

   var openRequest = indexedDB.open("mnm_images", dbVersion);
	
   //handle setup
		openRequest.onupgradeneeded = function(e) {
 
			console.log("running onupgradeneeded");
			var thisDb = e.target.result;
 
			//temp delete
			//thisDb.deleteObjectStore("note");
 
			//Create Note
			if(!thisDb.objectStoreNames.contains("pictures")) {
				console.log("I need to make the note objectstore");
				var objectStore = thisDb.createObjectStore("pictures");  
			}
 
 
		}
 
		openRequest.onsuccess = function(e) {
			db = e.target.result;
 
			db.onerror = function(event) {
			  // Generic error handler for all errors targeted at this database's
			  // requests!
			  alert("Database error: " + event.target.errorCode);
			  console.dir(event.target);
			};

			//displayNotes();
 
		}
	//var request = window.db.transaction(["note"], IDBTransaction.READ_WRITE)
		$('.hide').hide()


    $("#file_placebo").click(function () {
        $("#files").trigger('click');
    });

    $('#folder').change(function() {
    console && console.log($(this).val())
    
        if ($(this).val() === 'factory' || $(this).val() === 'choose' ) {
                $('.hide').hide()
            } else if($(this).val() != 'factory') {
                $('.hide').show()
            }
          
    });


    $(function() {
       $('#tabs, #tabs-1').tabs({
            activate: function(event, ui){
                var panelNumber = ui.newTab.index();
                //var panelNumber = ui.newPanel.index();
                var tabNumber = ui.newTab.index();

                var tabName = ui.newTab.text();
                console.log("tady")
                console && console.log(tabName);
                console && console.log(tabNumber, panelNumber);
                console && console.log(ui.newPanel.text())
                vypln = document.getElementById("tabs-" + panelNumber);
                
                delfile.server=tabName.trim()
                
                delfile.innerText="Erase selected files in any folder on " + tabName + " ?"
                console.log("delfile.server ", delfile.server)                
                
                //console && console.log("uvnitr ", vypln.innerHTML)

                //vypln.innerHTML += ' muj text ' + tabName
                //console && console.log(vypln.id)
            }
        });


     //   $("#sortable").sortable();
     //   $("#sortable").disableSelection();
    //    $("#sortable").sortable( "option", "containment", "div" );
    });


    $(function () {
        $("#tabs").tabs();
    });


    $('.ui-tabs-nav').on('click', 'li', function () { //print content sorting array
        //$("#tabs").tabs("option", "active", $(this).index());
    });

    function handleFileSelect(evt) {

        var path=''
        var uploadtype=''
        
        var folder=$('#folder').val()
        var items = ''
        if (folder=='custom'){
            path = '/usermedia/'
            uploadtype= 'image'
        }else if (folder=='programs'){
            path = '/programs/'
            uploadtype = 'file'
        }else if (folder=='external'){
            path = '/external/'
            uploadtype = 'image' 
        }else if (folder=='update'){
            path = '/update/'
            uploadtype='file'
        }


        //var files = document.getElementById('files').files;
        var files = evt.target.files
        console && console.log(files)

        if (!files.length) {
            alert('Please select a file!');
            return;
        }



//
        // Loop through the FileList and render image files as thumbnails.

//        for (var i = 0, f; f = files[i]; i++) { //ready for multiple files upload
            //	f=files[0] //for upload, single file only, to simplify upload for now
            // Only process image files.
            //if (!f.type.match('image.*')) {
            //  continue;
            //}

            var reader = new FileReader();
			var maxConnections = 18;
			var running = 0;

			function readFile(index){
				console && console.log("index, files.length ", index, files.length)
				while (running < maxConnections && index < files.length) {
				running+=1
				var f=files[index]

            document.getElementById('percent').className = 'percent'
            reader.onloadstart = function (e) {
                document.getElementById('progress_bar').className = 'loading';          
            };

            reader.onload = (function (theFile) {
				var theFile = theFile
                return function (e) {
                    var progress = document.querySelector('.percent');
                    if (document.update_servers.length == 0) {
                        progress.textContent = '100%';
                        progress.style.width = '100%';
                        progress.textContent = 'No host selected!';
                    }
                    for (var s = 0; s < document.update_servers.length; s++) {
                        (function (s) {
                            progress.style.width = '0%';
                            progress.textContent = '0%';
                            progress.textContent = 'Loading...';
                            var datablob = ''
                            datablob = e.target.result
                            //console && console.log("filereader ", datablob)
                            var ftp3 = new Ftp(document.update_servers[s], 21, console, document.getElementById("username").value, document.getElementById("password").value);
                            if (datablob.length > 46080000 || $('#blocking').is(':checked')) {
                                progress.style.width = '50%';
                                progress.textContent = 'Loading... blocking style upload, please wait...';
                                console && console.log(theFile.name)
                                    ftp3.upload_block(datablob, path + (theFile.name), function () {
                                    console && console.log("uploaded")
									running-=1
									readFile(index+1)
									if (index+1==files.length){
                                    setTimeout(function () {
                                    if (uploadtype=='file'){
                                        generate_file_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {})
                                    
                                    }else{
                                        generate_image_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {
                                            get_thumbnails(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), file_list)
                                            })
                                    }
                                    }, 1000)
									}
                                })
                            } else {
                                setTimeout(function(){
                                ftp3.upload_nb(datablob, path + (theFile.name), function () {
                                    console && console.log("uploaded")
									running-=1
									readFile(index+1)
									if (index+1==files.length){
                                    setTimeout(function () {
                                    if (uploadtype=='file'){
                                        generate_file_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {})
                                    }else{
                                        generate_image_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {
                                             get_thumbnails(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), file_list)
										 })
                                    }
                                    }, 1000) 
									}
                                })
                                },10)
                            }
                        })(s);
                    }
                };

			readFile(index+1)
            })(f); //on load
            reader.readAsBinaryString(f);
			} //while
			} // readFile
		readFile(0)
//        } // for in files
    }


    function toggleUpdate() { //update all toggle link

        var checkBoxes = $('.ui-tabs-nav .checkbox')
        checkBoxes.prop("checked", !checkBoxes.prop("checked")).trigger("change");
    }
    document.getElementById('toglik').addEventListener('click', toggleUpdate, false); //listener for click
    document.getElementById('files').addEventListener('change', handleFileSelect, false); //listener for file upload

    $(function () { //foldable sections
        $("#accordion").accordion({
            collapsible: true,
            active: false
        });
    });

    $('.ui-tabs-nav').on('click', '.ui-icon-refresh', function () { //refresh content images, downloads content and displays it in the panel

        var attrValue = $(this).parent()
        console && console.log("refreshing ", attrValue.text())
        generate_file_list(attrValue.text(), attrValue.index(), 'update',function (file_lista) {})
        generate_file_list(attrValue.text(), attrValue.index(), 'programs',function (file_listb) {})
        generate_image_list(attrValue.text(), attrValue.index(),'factory' ,function (file_listc) {
            get_thumbnails(attrValue.text(), attrValue.index(), file_listc)
        })
        
        generate_image_list(attrValue.text(), attrValue.index(),'custom' ,function (file_listd) { 
                    get_thumbnails(attrValue.text(), attrValue.index(), file_listd)
                     })
        generate_image_list(attrValue.text(), attrValue.index(),'external' ,function (file_liste) { 
                    get_thumbnails(attrValue.text(), attrValue.index(), file_liste)
                     })
        
    })

    $('.ui-tabs-nav').on('click', '.ui-icon-transfer-e-w', function () { //print content sorting array

        var attrValue = $(this).parent()
        var sortable_class = "#sortable.ui-sortable-" + attrValue.index()
        var sortedIDs = $(sortable_class).sortable("toArray", {
            attribute: 'id'
        });
        //minimelist.innerText = sortedIDs
    });


    $('.ui-tabs-nav').on('change', '#checkbox', function () {
        var attrValue = $(this).parent()
        x = $(this)
        console && console.log(attrValue.text(), x.val())
        //$('a',x.parent()).text()
//        delfile.server=$('a',x.parent()).text()
//        delfile.innerText="Erase selected files on " + delfile.server
        //attrValue.text()
        var index = document.update_servers.indexOf($('a', x.parent()).text())

        if (x.is(':checked')) {
            console && console.log(1)
            if (index < 0) document.update_servers.push($('a', x.parent()).text()) //add to array
        } else {
            console && console.log(0)
            if (index > -1) document.update_servers.splice(index, 1); //remove from array
        }

        $("#update_servers_span").text('' + document.update_servers);

    });


    $(function () {
        $(document).tooltip();
    });

    $('#blocking').on('change', function () {
        var attrValue = $(this).parent()
        x = $(this)
        console && console.log(attrValue.text(), x.is(':checked'))
    });

    $.expr[':'].textEquals = function (a, i, m) {
        return $(a).text().match("^" + m[3] + "$");
    };


    function addElement(server, dmx, callback) { //add tab dynamically

        console && console.log("adding element ", $('#tabs >ul >li').size())
        var tabs = $("#tabs").tabs();
        var label = server
        var id = $('#tabs >ul >li').size()
        var t_id = "tabs-" + id

        var tabTemplate = "<li><input type='checkbox' name='checkbox' value='1' id='checkbox' \
        class='checkbox' checked><a href='#{href}'>#{label}</a><span class='ui-icon ui-icon-refresh'> \
        </span><!--<span class='ui-icon ui-icon-transfer-e-w'></span>--> <canvas id='display-" + id + "' width='100' \
        height='18'></canvas> </li>"
        
        var li = $(tabTemplate.replace(/#\{href\}/g, "#" + t_id).replace(/#\{label\}/g, label))

        $("#tabs > .ui-tabs-nav").append(li);
        tabs.append("<div id='" + t_id + "' class='" + t_id + "'><ul></ul></div>");

        tabs.tabs("refresh");
        var in_update_servers = document.update_servers.indexOf(server)
        if (in_update_servers < 0) document.update_servers.push(server) //add to array as the host is being added checked
        var display = new SegmentDisplay("display-" + id);
        display.pattern = "###";
        display.displayAngle = 10;
        display.digitHeight = 10;
        display.digitWidth = 6;
        display.digitDistance = 2.5;
        display.segmentWidth = 1.3;
        display.segmentDistance = 0.5;
        display.segmentCount = 7;
        display.cornerType = 0;
        display.colorOn = "#ff0000";
        display.colorOff = "#ffffff";
        display.draw();
        display.setValue(dmx);


        var folders = $('#' + t_id).tabs();
        folders.find('.ui-tabs-nav').append("<li><a href='#factory' class='folder_click' value='factory'>Factory content</a></li>");
        folders.find(".ui-tabs-nav").append("<li><a href='#custom' class='folder_click' value='custom'>Custom content</a></li>");
        folders.find(".ui-tabs-nav").append("<li><a href='#external' class='folder_click' value='external'>External content</a></li>");
        folders.find(".ui-tabs-nav").append("<li><a href='#programs' class='folder_click' value='programs'>Programs folder</a></li>");
        folders.find(".ui-tabs-nav").append("<li><a href='#update' class='folder_click' value='update'>Updates folder</a></li>");

        folders.append("<div id='factory' class='factory'></div>");
        folders.append("<div id='custom' class='custom'></div>");
        folders.append("<div id='update' class='update'></div>");
        folders.append("<div id='external' class='external'></div>");
        folders.append("<div id='programs' class='programs'></div>");

        folders.tabs("refresh");

        $("#tabs").tabs("option", "active", id);
        
        var index = document.update_servers.indexOf(server)
        if (index < 0) document.update_servers.push(server) //add to array
        $("#update_servers_span").text('' + document.update_servers);



        
        
        
            $(function() {

//        $( ".folder_click").unbind( "click",function(){
        
        $('.folder_click').on('click', function () {
        console.log("zmen tab")
        $('#folder').val($(this).attr('value')).trigger('change');


        });

        
        
//        })
        



})

        

        console && console.log("added element ", id)
        callback(id)

    }

    //    minimelist = document.getElementById("result"); //result text area to print messages to

    var platform = ''
    chrome.runtime.getPlatformInfo(function (info) { //get platform
        platform = info.os
        console && console.log(platform);
        //minimelist.innerText = platform

    });

    var local = '' //set ip to bind to

    socket = chrome.socket;
    socket.create('udp', {}, function (createInfo) {
        bound_socket = createInfo.socketId;

        //platform="win"
        if (platform == "linux") {

            console && console.log("if linux")
            socket.getNetworkList(function (interfaces) { //get local network interfaces
                //for(var i in interfaces) {
                //var interface = interfaces[i];
                //var opt = document.createElement("option");
                //opt.value = interface.address;
                //opt.innerText = interface.name + " - " + interface.address;
                localhost = interfaces[0].address
                document.getElementById("host").value = localhost
                //minimelist.innerText += " , " + localhost
                //hosts.appendChild(opt);
                //}
            });
            $('#ping').append('<button class="btn btn-primary fix" id="parse">Parse ping!</button>')
            $("#ping").append('<textarea id="hosts" name="textarea" style="width:700px;height:50px;"></textarea>')

            parse.onclick = function () { //search hosts button

                console && console.log("parsing")
                to_parse = document.getElementById("hosts").value

                var host_row = to_parse.split(/[\r\n]+/) //array with rows of hosts

                for (var ax = 0; ax < host_row.length; ax++) {
                    (function (ax) { //closure for ax
                        if (host_row[ax].split(/\s+/)[0] == '64') { //filer out bad lines, 64 bytes from 192.168.20.1: icmp_seq=1 ttl=64 time=1.73 ms
                            var ho = host_row[ax].split(/\s+/)[3].slice(0, -1)
                            if (minimes.indexOf(ho) < 0) {
                                console && console.log("adding:", ho)
                                    scan_host(ho)
                            }
                        }
                    })(ax)
                }
            }
        } else {
            document.getElementById("host").value = "2.255.255.255"
            console && console.log("if win")
        }
        socket.bind(bound_socket, '0.0.0.0', 6454, function (result) {}); //bind socket
    });


    function ab2str(buf) {
        return String.fromCharCode.apply(null, new Uint16Array(buf));
    }

    function ab28str(buffer) {
        return String.fromCharCode.apply(null, new Uint8Array(buffer));
    };

    function str2ab(str) {
        var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
        var bufView = new Uint16Array(buf);
        for (var i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

function zeroPad(num,count)
{
  var numZeropad = num + '';
  while(numZeropad.length < count) {
    numZeropad = "0" + numZeropad;
  }
  return numZeropad;
}

    function generate_image_list(server, id, folder, callback) { //list files on ftp server, makes preview icons with simple thumbnail
        var path =''
        if (folder=='factory'){
            path = '/media/'
        }else if (folder=='custom'){
            path = '/usermedia/'
        }else if (folder=='external'){
            path = '/external/'
        }

        console && console.log("generate image list: ", server, id, folder, path)
        var ftp = new Ftp(server, 21, console, document.getElementById("username").value, document.getElementById("password").value);
        ftp.listDirectory(path, function (items) {
            var vypln_id = '#tabs-' + id + ' #'+folder
            var remove_id = '#tabs-' + id + ' #' + folder +' #tmp'
            var vypln = $(vypln_id)
            var remove_el = $(remove_id)
            var newtext = ''

            for (i = 0; i < items.length; i++) {
                if (i == 0) {
                    newtext = '<div id="tmp"><ul id="sortable" class="ui-sortable-' + id + '">'
                }
                newtext += '<li class="ui-state-default file-select ' + server.replace(/\./g,'-') +'" id="' + items[i] + '" titsle="' + zeroPad(i+1,3) +' : ' + items[i].replace(path,'') + '"> \
                <img src="icon-128.png" width="100" height="70" id="' + server + '-' + items[i] + '" ><span class="dmx_address" id="dmx_address">' + zeroPad(i+1,3) + '</span></li>'
                if (i == items.length - 1) {
                    newtext += '</ul></div>'
                    if (remove_el) remove_el.remove()
                    vypln.append(newtext)
                    var sortable_class = ".ui-sortable-" + id
                    var sortable_id = "#sortable" + sortable_class
                    $(sortable_id).sortable({
                        connectWith: sortable_class,
                        delay: 200
                    }).disableSelection();
                    //var sortedIDs = $(sortable_class).sortable("toArray", {
                    //    attribute: 'id'
                    //});
                    callback(items)
                }
            }
            
      $(".file-select").each(function (e) {
      
      this.onclick = function(e){
            console.log("SELECT", $(this))
            console.log($(this).parent().parent().parent().attr('id'))
            
        document.erasing=0
        $(delfile).removeClass('red')
            
        if ( $(this).parent().parent().parent().attr('id') != "factory" ){

        if (e.ctrlKey || e.metaKey) {
            $(this).children("span").toggleClass("selected")
        } else if (e.shiftKey) {
            $(this).children("span").addClass("selected").parent().siblings().children("span").addClass('selected')
            
        } else {
            $(this).children("span").toggleClass("selected").parent().siblings().children("span").removeClass('selected')
        }
        console.log( $(this).children("span"))
//                    xxx=$(this).children('span')

        }            

      }
      
    })
    
    $( "li" ).hover(
  function() {
    $( "#status-bar" ).text( ($(this).attr("id")) );
  }, function() {
    $( "#status-bar" ).text( ".");
  }
);

        })
        
      

        
    }


    function generate_file_list(server, id, folder, callback) { //list files on ftp server, makes preview icons with simple thumbnail
        var path =''

        if (folder=='update'){
            path = '/update/'
        }else if (folder=='programs'){
            path = '/programs/'
        }

        console && console.log("generate file list: ", server, id, id, folder)
        var ftp = new Ftp(server, 21, console, document.getElementById("username").value, document.getElementById("password").value);

        ftp.listDirectory(path, function (items) {
            var vypln_id = '#tabs-' + id + ' #' + folder
            var remove_id = '#tabs-' + id + ' #'+ folder +' #tmp'
            var vypln = $(vypln_id)
            var remove_el = $(remove_id)
            var newtext = ''

            for (i = 0; i < items.length; i++) {
                if (i == 0) {
                    newtext = '<div id="tmp"><ul id="file_sortable">'
                }
                newtext += '<li class="ui-state-default file-select ' + server.replace(/\./g,'-') +'" id="' + items[i] + '"><span class="dmx_address file_address" id="dmx_address">' + items[i] + '</span></li>'
                if (i == items.length - 1) {
                    newtext += '</ul><tmp>'
                    //                    vypln.innerHTML = newtext
                    if (remove_el) remove_el.remove()
                    vypln.append(newtext)
                    callback(items)
                }
            }
            
      $(".file-select").each(function (e) {
      
      this.onclick = function(e){
            console.log("SELECT", $(this))
            console.log($(this).parent().parent().parent().attr('id'))
            
        if ( $(this).parent().parent().parent().attr('id') != "factory" ){

        if (e.ctrlKey || e.metaKey) {
            $(this).children("span").toggleClass("selected")
        } else if (e.shiftKey) {
            $(this).children("span").addClass("selected").parent().siblings().children("span").addClass('selected')
            
        } else {
            $(this).children("span").toggleClass("selected").parent().siblings().children("span").removeClass('selected')
        }
        console.log( $(this).children("span"))
//                    xxx=$(this).children('span')

        }            

      }
      
    })

        })
    }

    pingall.onclick = function () { //search hosts button

        host = document.getElementById("host").value;
        scan_host(host)
    };

    adddb.onclick = function(){

	var request = db.transaction(["pictures"], 'readwrite').objectStore("aaa").put(document.getElementById("host").value,document.getElementById("host").value);

	

}
    delall.onclick = function () {
    
        if (document.erase_all ==0) {
        document.erase_all=1
        $(delall).addClass('red')
        
        setTimeout(function(){
            document.erase_all=0
            $(delall).removeClass('red')
        },5000)
        
        }else{
    
        var uploadtype=''
        var path = ''
        
        var folder=$('#folder').val()
        var items = ''
        if (folder=='custom'){
            path = '/usermedia/'
            uploadtype='image'
        }else if (folder=='programs'){
            path = '/programs/'
            uploadtype='file'
        }else if (folder=='external'){
            path = '/external/'
            uploadtype='image'
        }else if (folder=='update'){
            path = '/update/'
            uploadtype='file'
        }

        console && console.log("dell all", document.update_servers.length)
        var progress = document.querySelector('.percent');
        if (document.update_servers.length == 0) {
            progress.textContent = '100%';
            progress.style.width = '100%';
            progress.textContent = 'No host selected!';
        }

        for (var s = 0; s < document.update_servers.length; s++) {
            (function (s) {
                var ftp4 = new Ftp(document.update_servers[s], 21, console, document.getElementById("username").value, document.getElementById("password").value);
                ftp4.listDirectory(path, function (items) {
                    (function (items) {
                        console && console.log(items)
                        for (var it = 0; it < items.length; it++) {
                            (function (it) {
                                console && console.log(items[it])
                                ftp4.deleteFile(items[it], function () {})

                            })(it)
                        }
                    })(items)
                })
                setTimeout(function () {
                    if (uploadtype=='file'){
                    
                        generate_file_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {})

                    }else{
                        generate_image_list(document.update_servers[s], $('#tabs li a:textEquals(' + document.update_servers[s] + ')').parent().index(), folder,function (file_list) {})

                    }
                    
                }, 2000)
            })(s);
        }
            document.erase_all=0
            $(delall).removeClass('red')

        }
    }


    list.onclick = function () { //add individual host button
        console && console.log('clisk')
        add_host(document.getElementById("host").value)
    }

    function get_thumbnails(server, panel_id, file_list) {
		var server = server
		var panel_id = panel_id
		var maxConnections = 5;
		var running = 0;
		var file_list = file_list
		console && console.log("generate  thumbnails: ", server, panel_id, file_list, file_list.length)

		var ftp2 = new Ftp(server, 21, console, document.getElementById("username").value, document.getElementById("password").value);

				function flush(){
					while (running < maxConnections && file_list.length) {
						running	+=1
						var get_file = file_list.shift()
						function proceed (get_file){
							//console && console.log(get_file, file_list)
							console.log("restireve ", get_file)
				
							ftp2.retrieve(get_file, function (blob) {
								var img = document.getElementById(server + '-' + get_file);
								//console && console.log(img)
								//img.src = 'data:image/png;base64,' + btoa(blob);
								qqq='data:image/png;base64,' + btoa(blob);
								img.src= 'data:image/png;base64,' + _resize(qqq,100,70) //resize image to 100x70
								running-=1
								flush()
							})
						}
						proceed(get_file)
					
					}
					}
	flush()
					
    }

    function add_host(host, dmx) {
        console && console.log("adding host", host)
        dmx = dmx || ""
        addElement(host, dmx, function (panel_id) {
            console && console.log('element added: ', panel_id)
            generate_file_list(host, panel_id, 'update',function (file_lista) {})
            generate_file_list(host, panel_id, 'programs',function (file_listb) {})
            generate_image_list(host, panel_id, 'factory', function (file_listc) {
                get_thumbnails(host, panel_id, file_listc)
            })
            generate_image_list(host, panel_id,'custom' ,function (file_listd) { 
                get_thumbnails(host, panel_id, file_listd)
             })
            generate_image_list(host, panel_id,'external' ,function (file_liste) { 
                get_thumbnails(host, panel_id, file_liste)
                 })

        });
    }


    function scan_host(host) {


        console && console.log("host: ", host)
        console && console.log("pingall")
        var data = new Uint8Array([65, 114, 116, 45, 78, 101, 116, 0x00, 0x00, 0x20, 0x00, 0x0e, 0x01, 0x00]) //ArtPoll discovery packet
            function read() { //read from socket
                socket.recvFrom(bound_socket, 1024, function (recvFromInfo) {
                    console && console.log("received: ", recvFromInfo);
                    console && console.log("code: ", recvFromInfo.resultCode);

                    if (recvFromInfo.resultCode >= 0) {
                        if (minimes.indexOf(recvFromInfo.address) < 0) {
                            console && console.log("found new host:", recvFromInfo);
                            var reply = ab28str(recvFromInfo.data)
                            var vzor = 'Art-Net'
                            var shoda = 0
                            var dmx = ''
                            if (reply.length > 10) {
                                for (var ar = 0; ar < 10; ar++) {
                                    if (reply[ar] == vzor[ar]) shoda++
                                }
                                if (shoda == 7) {
                                    console && console.log("is artnet ", reply)
                                    xxx=reply
                                    //dmx = reply.slice(68, 71).toString()
                                    
                                    //dmx = reply.slice(reply.indexOf("Dmx:")+4,reply.indexOf("Dmx:")+7)
                                    dmx = reply.slice(reply.search(/dmx:/i)+4,reply.search(/dmx:/i)+9).trim() //ignore case, ignore white space
                                    // if (dmx.length == '5') dmx = '000'
                                }
                            }

                            minimes.push(recvFromInfo.address) //add to array
                            add_host(recvFromInfo.address.toString(), dmx)
                        }
                        read()
                    }
                });
            }
        read();


        socket.sendTo(bound_socket, data.buffer, host, 6454, function (writeInfo) { //send ArtPoll
            if (writeInfo.bytesWritten != data.byteLength) {
                console && console.log(writeInfo.bytesWritten, data.byteLength);
            } else {
                read()
            }
        });
    }

    closit.onclick = function () {
        console && console.log("close")
        socket.destroy(bound_socket)
        window.close()
    }
  
  
  function _resize(blob, maxWidth, maxHeight) 
{
        var ratio = 1;

    var img = new Image
    img.src=blob
	var canvas = document.createElement("canvas");
	canvas.style.display="none";
	document.body.appendChild(canvas);

	var canvasCopy = document.createElement("canvas");
	canvasCopy.style.display="none";
	document.body.appendChild(canvasCopy);

	var ctx = canvas.getContext("2d");
	var copyContext = canvasCopy.getContext("2d");

        if(img.width > maxWidth)
                ratio = maxWidth / img.width;
        else if(img.height > maxHeight)
                ratio = maxHeight / img.height;

        canvasCopy.width = img.width;
        canvasCopy.height = img.height;
try {
        copyContext.drawImage(img, 0, 0);
} catch (e) { 
//	document.getElementById('loader').style.display="none";
    console.log("There was a problem - please reupload your image");
	return false;
}

        canvas.width = img.width * ratio;
        canvas.height = img.height * ratio;
        // the line to change
        //ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
        // the method signature you are using is for slicing
        ctx.drawImage(canvasCopy, 0, 0, canvas.width, canvas.height);

    	var dataURL = canvas.toDataURL("image/png");

	    document.body.removeChild(canvas);
	    document.body.removeChild(canvasCopy);
    	return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");


};
    
    
    delfile.onclick = function(){
    
    if ($('.' + delfile.server.replace(/\./g,'-') + ' .selected').length > 0){
    
    
    
    if (document.erasing ==0) {
        $('.' + delfile.server.replace(/\./g,'-') + '.file-select').not($('.selected').parent()).hide()    
        document.erasing=1
        $(delfile).addClass('red')
        
        setTimeout(function(){
            document.erasing=0
            $(delfile).removeClass('red')
            $('.' + delfile.server.replace(/\./g,'-') + '.file-select').not($('.selected').parent()).show()    

        },5000)
        
    }else{
    var ftp4 = new Ftp(delfile.server, 21, console, document.getElementById("username").value, document.getElementById("password").value);

    for (var ii=0; ii < $('.selected').length;ii++){

        (function (ii){
            console.log("erasing: ",$($('.' + delfile.server.replace(/\./g,'-') + ' .selected')[ii]).parent().attr('id'))
            ftp4.deleteFile($($('.' + delfile.server.replace(/\./g,'-') + ' .selected')[ii]).parent().attr('id'), function () {})
            
        })(ii)
        

    }
    
        
//        aaa=$(this)
//        console.log(this.parent().attr('id'))
        
        
//        console.log("erasing")
//        console.log($('.selected'))
        document.erasing=0
        $(delfile).removeClass('red')

        setTimeout(function () {

                generate_file_list(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), 'update',function (file_lista) {})
                generate_file_list(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), 'programs',function (file_listb) {})

              generate_image_list(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), 'factory',function (file_listo) {
                get_thumbnails(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), file_listo)
                })
            
              generate_image_list(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), 'custom',function (file_listi) {
                get_thumbnails(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), file_listi)
                })

              generate_image_list(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), 'external',function (file_listx) {
                get_thumbnails(delfile.server, $('#tabs li a:textEquals(' + delfile.server + ')').parent().index(), file_listx)
                })

            
        }, 2000)
   
        
    }
    }
    
    }

    chrome.app.window.onClosed.addListener(function (windowId) {
        socket.destroy(bound_socket)
        //    console && console.log("closing time for ", bound_socket)
    })
};
