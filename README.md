MNM - MNM is Not Manager
======================

This is MNM, test case of JavaScript based Chromium/Chrome packaged app. It utilizes chrome.socket heavily for UDP/TCP communication - broadcast based discovery and also for FTP transfer. It includes and utilizes JQuery and several other tools like segmented-display.js, modified ftp.js etc.

Screenshot:

![Screenshot](./screenshot.png "Screenshot")
